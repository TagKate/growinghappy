"""growinghappy_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from services.views import main_page, ServiceListView, ServiceDetailView, ServiceCreateView, ArticleListView, ArticleDetailView, ArticleCreateView, ServiceDeleteView, ArticleDeleteView, OrderCreateView, OrderListView, OrderDetailView, SuperuserLoginView, ServiceUpdateView, contacts_page

urlpatterns = [
    path('', main_page, name = 'main'),
    path('admin/', admin.site.urls),
    path('services', ServiceListView.as_view(), name ='services'),
    path('services/<int:pk>', ServiceDetailView.as_view(), name='service'),
    path('services/create/', ServiceCreateView.as_view(), name='service_create'),
    path('articles', ArticleListView.as_view(), name ='articles'),
    path('articles/create/', ArticleCreateView.as_view(), name='article_create'),
    path('articles/<int:pk>', ArticleDetailView.as_view(), name='article'),
    path('services/delete/<int:pk>', ServiceDeleteView.as_view(), name='service_delete'),
    path('articles/delete/<int:pk>', ArticleDeleteView.as_view(), name='article_delete'),
    path('services/neworder/', OrderCreateView.as_view(), name='order_create'),
    path('orders_list', OrderListView.as_view(), name='orders_list'),
    path('orders/<int:pk>', OrderDetailView.as_view(), name='order'),
    path('superuser/login/', SuperuserLoginView.as_view(), name='superuserlogin'),
    path('services/update/<int:pk>/', ServiceUpdateView.as_view(), name='service_update'),
    path('contacts/', contacts_page, name='contacts'),
]
