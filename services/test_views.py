from django.contrib.auth.models import User
from django.test import TestCase

from services.models import Category, Service, Article


class TestServicesListView(TestCase):

    def test_response_status_code(self):
        response = self.client.get('/services')
        self.assertEqual(response.status_code, 200)

    def test_service_detail(self):
        response = self.client.get('/services/2222')
        self.assertEqual(response.status_code, 404)

        category = Category.objects.create(name='курс')
        service = Service.objects.create(name='Истерики: причины, что делать', category=category)

        response = self.client.get(f'/services/{service.pk}')
        self.assertEqual(response.status_code, 200)

    def test_permissions(self):
        # Это гость
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # Создаем пользователя
        #user = User.objects.create_superuser(username='user', email='user@email.com', password='user123456')
        #self.client.login(username='user', password='user123456')
        # Авторизованный запрос
        #response = self.client.get('/')
        #self.assertEqual(response.status_code, 200)

class TestArticlesListView(TestCase):

    def test_response_status_code(self):
        response = self.client.get('/articles')
        self.assertEqual(response.status_code, 200)

    def test_article_detail(self):
        response = self.client.get('/articles/2222')
        self.assertEqual(response.status_code, 404)

        article = Article.objects.create(articlename='К врачу без слез')

        response = self.client.get(f'/articles/{article.pk}')
        self.assertEqual(response.status_code, 200)

class TestMainView(TestCase):

    def test_response_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_content_services(self):
        response = self.client.get('/')
        button = '<a class="nav-link" href="/services">Услуги</a>'.encode(encoding='utf-8')
        self.assertIn(button, response.content)

    def test_content_articles(self):
        response = self.client.get('/')
        button = '<a class="nav-link" href="/articles">Полезные статьи</a>'.encode(encoding='utf-8')
        self.assertIn(button, response.content)

class TestSuperuserView(TestCase):

    def test_response_status_code(self):
        response = self.client.get('/superuser/login/')
        self.assertEqual(response.status_code, 200)