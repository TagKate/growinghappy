# Generated by Django 4.1.7 on 2023-02-23 21:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0007_rename_ordername_order_имя_remove_order_category_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='comment',
            new_name='Комментарий',
        ),
        migrations.RenameField(
            model_name='order',
            old_name='cellphone',
            new_name='Телефон',
        ),
        migrations.RenameField(
            model_name='order',
            old_name='servicename',
            new_name='Услуга',
        ),
    ]
