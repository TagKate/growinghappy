from django.db import models
from django.template.defaultfilters import join


# Create your models here.

class Category (models.Model):
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return self.name

class Service (models.Model):
    name = models.CharField(max_length=64, verbose_name='Название')
    category = models.ForeignKey(Category, on_delete=models.PROTECT, verbose_name='Категория')

    def __str__(self):
        return self.name

class Article (models.Model):
    articlename = models.CharField(max_length=150)
    articletext = models.TextField(verbose_name='articletext', blank=False)

    def __str__(self):
        return self.articlename

class Order (models.Model):
    Имя = models.CharField(max_length=64)
    Телефон = models.DecimalField(max_digits=11, decimal_places=0)
    Услуга = models.ForeignKey(Service, on_delete=models.PROTECT)
    Комментарий = models.TextField(blank=True)