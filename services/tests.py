from django.test import TestCase
from .models import Category, Service, Article


# Create your tests here.

class TestCategory(TestCase):

    def setUp(self) -> None:
        self.category = Category.objects.create(name='курс')
        print('Я выполняюсь ПЕРЕД каждым тестом')

    def tearDown(self) -> None:
        print('Я выполняюсь ПОСЛЕ каждого теста')

    def test_init(self):
        self.assertTrue(isinstance(self.category.name, str))
        self.assertEqual(self.category.name, 'курс')

    def test_str(self):
        category = Category.objects.get(name='курс')
        self.assertEqual(str(category), 'курс')

class TestService(TestCase):
    def test_str(self):
        category = Category.objects.create(name='курс')
        service = Service.objects.create(name='Истерики: причины, что делать', category=category)
        self.assertEqual(str(service), 'Истерики: причины, что делать')

class TestArticle(TestCase):
    def test_str(self):
        article = Article.objects.create(articlename='К врачу без слез')
        self.assertEqual(str(article), 'К врачу без слез')