from django.contrib.auth.views import LoginView
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView

from .models import Service, Article, Order


# Create your views here.

def main_page(request):
    return render(request, 'services/index.html')

def contacts_page(request):
    return render(request, 'services/contacts.html')

class ServiceDetailView (DetailView):
    model = Service

class ServiceListView (ListView):
    model = Service

class ServiceCreateView (CreateView):
    model = Service
    fields = ('name', 'category')
    success_url= reverse_lazy('services')

class ArticleDetailView (DetailView):
    model = Article

class ArticleListView (ListView):
    model = Article

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['help_text'] = 'хэлптекст'
        return context

class ArticleCreateView (CreateView):
    model = Article
    fields = ('articlename', 'articletext')
    success_url= reverse_lazy('articles')

class ServiceDeleteView (DeleteView):
    model = Service
    success_url = reverse_lazy('services')

class ArticleDeleteView (DeleteView):
    model = Article
    success_url = reverse_lazy('articles')

class OrderCreateView (CreateView):
    model = Order
    fields = ('Имя', 'Телефон', 'Услуга', 'Комментарий')
    success_url = reverse_lazy('main')

class OrderListView (ListView):
    model = Order

class OrderDetailView (DetailView):
    model = Order

class SuperuserLoginView (LoginView):
    template_name = 'services/superuser_loginform.html'

class ServiceUpdateView (UpdateView):
    model = Service
    fields = '__all__'
    success_url = reverse_lazy('services')

